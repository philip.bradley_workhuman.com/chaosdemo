# ChaosDemo

The purpose of this project is to demonstrate the use of the [Chaos Monkey module for Spring Boot](https://codecentric.github.io/chaos-monkey-spring-boot/). The source code for this module can be found here

[https://github.com/codecentric/chaos-monkey-spring-boot](https://github.com/codecentric/chaos-monkey-spring-boot)

The project consists of a skeleton Spring Data JDBC entity (Product) which is persisted via ProductRepository. When the chaos monkey is enabled, any calls to a JDBC repository can potentially be intercepted by the chaos monkey which will throw an exception. 


~~~
spring.profiles.active=chaos-monkey
chaos.monkey.enabled=true
chaos.monkey.watcher.repository=true

chaos.monkey.assaults.exceptions-active=true
chaos.monkey.assaults.level=3
~~~

The `RepositoryChaosTests` JUnit test attemps to insert a number of records in the face of the chaos monkey generating run time errors. The test case validates that all of the expected records get inserted. 


An overview of project can be found here
[Chaos Testing](src/main/resources/docs/Chaos_Testing.pptx)