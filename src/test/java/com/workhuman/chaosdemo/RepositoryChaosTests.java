package com.workhuman.chaosdemo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import com.workhuman.chaosdemo.domain.Product;
import com.workhuman.chaosdemo.repository.ProductRepository;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = {ProductRepository.class})
@EnableAutoConfiguration
@TestPropertySource(locations="classpath:chaos-test.properties")
class RepositoryChaosTests {

	private static final Logger LOG = LoggerFactory.getLogger(RepositoryChaosTests.class);
	
	@Autowired
	private ProductRepository productRepository;
		
	@Test
	void insertWithRecoverableErrors() {
		
		int recordCount = 1000;
		List<Product> products = createProducts(recordCount);
		LOG.info("Created {} records", products.size());

		int insertCount = 0;
		int errorCount = 0;
		
		while (!products.isEmpty()) {
			Product product = products.get(products.size()-1);
			
			try {
				Product saved = productRepository.save(product);
				products.remove(products.size()-1);
				insertCount++;
				LOG.info("Insert count {}, Saved: {}", insertCount, saved);
			} catch (RuntimeException ex) {
				errorCount++;
				LOG.error("Insert failed: Error count: {} ", errorCount);
			}
		}
				
		assertEquals(recordCount, insertCount, "Validate insert count");
		LOG.info("Inserted {} records with {} errors", insertCount, errorCount);
	}
	
	private List<Product> createProducts(int size) {
		
		List<Product> products = new ArrayList<>(size);
		
		for (int i=1; i<=size; i++) {
			products.add(new Product("Product " + i));
		}
		
		return products;
	}
}
