package com.workhuman.chaosdemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.workhuman.chaosdemo.domain.Product;
import com.workhuman.chaosdemo.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest(classes = {ProductRepository.class})
@EnableAutoConfiguration
public class RepositoryTests {

    private static final Logger LOG = LoggerFactory.getLogger(RepositoryTests.class);

    @Autowired
    private ProductRepository productRepository;

    @Test
    void productDetailsAreSaved() {
        Product saved = productRepository.save(new Product("My Test Product"));
        Optional<Product> retrieved = productRepository.findById(saved.getId());

        assertTrue(retrieved.isPresent());
        assertEquals(retrieved.get().getName(), saved.getName());
    }
}
