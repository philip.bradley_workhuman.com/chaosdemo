package com.workhuman.chaosdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@SpringBootApplication
@EnableJdbcRepositories
public class ChaosDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaosDemoApplication.class, args);
	}

}
