package com.workhuman.chaosdemo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workhuman.chaosdemo.domain.Product;
import com.workhuman.chaosdemo.repository.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductController {

	private ProductRepository productRepository;

	@Autowired
	public ProductController(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> findById(@PathVariable Long id) {

		Optional<Product> result = productRepository.findById(id);

		if (result.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(result.get());
	}
}